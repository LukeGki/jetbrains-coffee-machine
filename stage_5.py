water = 400
milk = 540
coffee_beans = 120
disposable_cups = 9
money = 550


def buy():
    global water, milk, coffee_beans, disposable_cups, money
    print("What do you want to buy? 1 - espresso, 2 - latte, 3 - cappuccino, , back - to main menu:")
    coffee_product = input()
    if coffee_product == "back":
        return None
    elif int(coffee_product) == 1:
        water_cost = 250
        milk_cost = 0
        coffee_beans_cost = 16
        price = 4
    elif int(coffee_product) == 2:
        water_cost = 350
        milk_cost = 75
        coffee_beans_cost = 20
        price = 7
    elif int(coffee_product) == 3:
        water_cost = 200
        milk_cost = 100
        coffee_beans_cost = 12
        price = 6
    if water >= water_cost and milk >= milk_cost and coffee_beans >= coffee_beans_cost and disposable_cups >= 1:
        print("I have enough resources, making you a coffee!")
        water -= water_cost
        milk -= milk_cost
        coffee_beans -= coffee_beans_cost
        disposable_cups -= 1
        money += price
    else:
        if water < water_cost:
            print("Sorry, not enough water!")
        if milk < milk_cost:
            print("Sorry, not enough milk!")
        if coffee_beans < coffee_beans_cost:
            print("Sorry, not enough coffee beans!")
        if disposable_cups < 1:
            print("Sorry, not enough disposable cups!")


def fill():
    global water, milk, coffee_beans, disposable_cups
    print("Write how many ml of water do you want to add:")
    fill_water = int(input())
    water += fill_water
    print("Write how many ml of milk do you want to add:")
    fill_milk = int(input())
    milk += fill_milk
    print("Write how many grams of coffee beans do you want to add:")
    fill_coffee_beans = int(input())
    coffee_beans += fill_coffee_beans
    print("Write how many disposable cups of coffee do you want to add:")
    fill_disposable_cups = int(input())
    disposable_cups += fill_disposable_cups


def take():
    global money
    take_money = money
    money = 0
    print(f"I gave you ${take_money}")


def remaining():
    print("The coffee machine has:")
    print(water, "of water")
    print(milk, "of milk")
    print(coffee_beans, "of coffee beans")
    print(disposable_cups, "of disposable cups")
    print(money, "of money")


while True:
    print("Write action (buy, fill, take, remaining, exit):")
    action = input()
    if action == "buy":
        buy()
    elif action == "fill":
        fill()
    elif action == "take":
        take()
    elif action == "remaining":
        remaining()
    elif action == "exit":
        break
