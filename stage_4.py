water = 400
milk = 540
coffee_beans = 120
disposable_cups = 9
money = 550

def buy():
    global water, milk, coffee_beans, disposable_cups, money
    print("What do you want to buy? 1 - espresso, 2 - latte, 3 - cappuccino:")
    coffee_product = int(input())
    if coffee_product == 1:
        water -= 250
        coffee_beans -= 16
        disposable_cups -= 1
        money += 4
    elif coffee_product == 2:
        water -= 350
        milk -= 75
        coffee_beans -= 20
        disposable_cups -= 1
        money += 7
    elif coffee_product == 3:
        water -= 200
        milk -= 100
        coffee_beans -= 12
        disposable_cups -= 1
        money += 6

def fill():
    global water, milk, coffee_beans, disposable_cups
    print("Write how many ml of water do you want to add:")
    fill_water = int(input())
    water += fill_water
    print("Write how many ml of milk do you want to add:")
    fill_milk = int(input())
    milk += fill_milk
    print("Write how many grams of coffee beans do you want to add:")
    fill_coffee_beans = int(input())
    coffee_beans += fill_coffee_beans
    print("Write how many disposable cups of coffee do you want to add:")
    fill_disposable_cups = int(input())
    disposable_cups += fill_disposable_cups

def take():
    global money
    take_money = money
    money = 0
    print(f"I gave you ${take_money}")

print("The coffee machine has:")
print(water, "of water")
print(milk, "of milk")
print(coffee_beans, "of coffee beans")
print(disposable_cups, "of disposable cups")
print(money, "of money")

print("Write action (buy, fill, take):")
action = input()

if action == "buy":
    buy()
elif action == "fill":
    fill()
elif action == "take":
    take()

print("The coffee machine has:")
print(water, "of water")
print(milk, "of milk")
print(coffee_beans, "of coffee beans")
print(disposable_cups, "of disposable cups")
print(money, "of money")
