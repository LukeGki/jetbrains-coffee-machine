print("Write how many ml of water the coffee machine has:")
water = int(input())
print("Write how many ml of milk the coffee machine has:")
milk = int(input())
print("Write how many grams of coffee beans the coffee machine has:")
coffee_beans = int(input())
print("Write how many cups of coffee you will need:")
cups = int(input())

water_cups = water // 200
milk_cups = milk // 50
coffee_beans_cups = coffee_beans // 15
cups_possible = min(water_cups, milk_cups, coffee_beans_cups)

if cups < cups_possible:
    cups_more = cups_possible - cups
    print("Yes, I can make that amount of coffee (and even ", cups_more, "more than that)")
elif cups == cups_possible:
    print("Yes, I can make that amount of coffee")
else:
    print("No, I can make only ", cups_possible, " cups of coffee")
